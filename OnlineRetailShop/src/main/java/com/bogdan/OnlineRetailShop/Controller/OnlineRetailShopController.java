package com.bogdan.OnlineRetailShop.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.bogdan.OnlineRetailShop.Service.ProductService;

@Controller
@RequestMapping("OnlineRetailShopController")
public class OnlineRetailShopController {

	@Autowired
	ProductService productService;

	// http://localhost:8080/OnlineRetailShop/OnlineRetailShopController/
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String getHomePage() {

		return "pages/homePage";

	}

	// http://localhost:8080/OnlineRetailShop/OnlineRetailShopController/serachProduct/
	@RequestMapping(value = "/searchProduct", method = RequestMethod.GET)
	public String searchProduct(@RequestParam(value = "productName", required = false) String productName,
			ModelMap model) {

		// search for product
		model.addAttribute("productsList", productService.searchForProduct(productName));
		return "resultPage";
	}
}
