package com.bogdan.OnlineRetailShop.Dao;

import java.util.List;

import com.bogdan.OnlineRetailShop.Model.Category;
import com.bogdan.OnlineRetailShop.Model.Product;

public interface ProductDao {

	public void addProduct(Product product);

	public void updateProduct(Product product);

	public void deleteProduct(Product product);

	public List<Product> getProductList();

	public List<Product> getProductListForCategory(Category category);
}
