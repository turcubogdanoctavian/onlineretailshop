package com.bogdan.OnlineRetailShop.Dao;

import java.util.List;

import com.bogdan.OnlineRetailShop.Model.Customer;
import com.bogdan.OnlineRetailShop.Model.Payment;

public interface PaymentDao {
	public void addPayment(Payment payment);

	public List<Payment> getPaymentList();

	public List<Payment> getPaymentListForCustomer(Customer customer);
}
