package com.bogdan.OnlineRetailShop.Dao;

import java.util.List;

import com.bogdan.OnlineRetailShop.Model.Employee;

public interface EmployeeDao {

	public void addEmployee(Employee employee);

	public void updateEmployee(Employee employee);

	public void deleteEmployee(Employee employee);

	public List<Employee> getEmployeeList();
}
