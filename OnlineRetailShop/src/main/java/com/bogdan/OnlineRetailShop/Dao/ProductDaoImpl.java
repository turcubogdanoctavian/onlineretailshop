package com.bogdan.OnlineRetailShop.Dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bogdan.OnlineRetailShop.Model.Category;
import com.bogdan.OnlineRetailShop.Model.Product;

@Repository
@Transactional
public class ProductDaoImpl implements ProductDao {

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public void addProduct(Product product) {
		sessionFactory.getCurrentSession().save(product);

	}

	@Override
	public void updateProduct(Product product) {
		sessionFactory.getCurrentSession().update(product);

	}

	@Override
	public void deleteProduct(Product product) {
		sessionFactory.getCurrentSession().delete(product);

	}

	@Override
	public List<Product> getProductList() {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		@SuppressWarnings("unchecked")
		List<Product> productList = session.createCriteria("from Product").list();

		for (Product product : productList) {
			System.out.println(product);
		}
		transaction.commit();
		return productList;
	}

	@Override
	public List<Product> getProductListForCategory(Category category) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		Query query = sessionFactory.getCurrentSession().createQuery("select * from Product where category= :param")
				.setParameter("param", category);

		@SuppressWarnings("unchecked")
		List<Product> productList = query.list();

		for (Product product : productList) {
			System.out.println(product);
		}

		transaction.commit();
		return productList;
	}

}
