package com.bogdan.OnlineRetailShop.Dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bogdan.OnlineRetailShop.Model.Order;
import com.bogdan.OnlineRetailShop.Model.Product;
import com.bogdan.OnlineRetailShop.Model.ProductDetails;

@Repository
@Transactional
public class ProductDetailsDaoImpl implements ProductDetailsDao {

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public void addProductDetails(ProductDetails productDetails) {
		sessionFactory.getCurrentSession().save(productDetails);

	}

	@Override
	public void updateProductDetails(ProductDetails productDetails) {
		sessionFactory.getCurrentSession().update(productDetails);

	}

	@Override
	public void deleteProductDetails(ProductDetails productDetails) {
		sessionFactory.getCurrentSession().delete(productDetails);

	}

	@Override
	public ProductDetails getProductDetailsForProduct(Product product) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		ProductDetails productDetails = (ProductDetails) sessionFactory.getCurrentSession()
				.createQuery("from ProductDetails where product= :param").setParameter("param", product);

		transaction.commit();
		return productDetails;
	}

}
