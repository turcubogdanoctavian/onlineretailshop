package com.bogdan.OnlineRetailShop.Dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bogdan.OnlineRetailShop.Model.Customer;
import com.bogdan.OnlineRetailShop.Model.Order;

@Repository
@Transactional
public class OrderDaoImpl implements OrderDao {

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public void addOrder(Order order) {
		sessionFactory.getCurrentSession().save(order);

	}

	@Override
	public void updateOrder(Order order) {
		sessionFactory.getCurrentSession().update(order);

	}

	@Override
	public void deleteOrder(Order order) {
		sessionFactory.getCurrentSession().delete(order);

	}

	@Override
	public List<Order> getOrderList() {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		@SuppressWarnings("unchecked")
		List<Order> orderList = session.createCriteria("from Order").list();

		for (Order order : orderList) {
			System.out.println(order);
		}

		transaction.commit();
		return orderList;
	}

	@Override
	public List<Order> getOrderListForCustomer(Customer customer) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		Query query = sessionFactory.getCurrentSession().createQuery("select * from Order where customer= :param")
				.setParameter("param", customer);

		@SuppressWarnings("unchecked")
		List<Order> orderList = query.list();

		for (Order order : orderList) {
			System.out.println(order);
		}

		transaction.commit();
		return orderList;
	}

}
