package com.bogdan.OnlineRetailShop.Dao;

import java.util.List;

import com.bogdan.OnlineRetailShop.Model.Customer;

public interface CustomerDao {

	public void addCustomer(Customer customer);

	public void updateCustomer(Customer customer);

	public List<Customer> getCustomerList();

}
