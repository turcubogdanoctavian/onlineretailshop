package com.bogdan.OnlineRetailShop.Dao;

import java.util.List;

import com.bogdan.OnlineRetailShop.Model.Category;

public interface CategoryDao {

	public void addCategory(Category category);

	public void updateCategory(Category category);

	public void deteleCategory(Category category);

	public List<Category> getCategoryList();

}
