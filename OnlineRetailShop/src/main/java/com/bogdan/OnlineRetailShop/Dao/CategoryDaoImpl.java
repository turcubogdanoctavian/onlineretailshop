package com.bogdan.OnlineRetailShop.Dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bogdan.OnlineRetailShop.Model.Category;

@Repository
@Transactional
public class CategoryDaoImpl implements CategoryDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void addCategory(Category category) {
		sessionFactory.getCurrentSession().save(category);

	}

	@Override
	public void updateCategory(Category category) {
		sessionFactory.getCurrentSession().update(category);

	}

	@Override
	public void deteleCategory(Category category) {
		sessionFactory.getCurrentSession().delete(category);

	}

	@Override
	public List<Category> getCategoryList() {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		@SuppressWarnings("unchecked")
		List<Category> categoryList = session.createQuery("from Category").list();

		for (Category category : categoryList) {
			System.out.println(category);
		}

		transaction.commit();
		return categoryList;
	}

}
