package com.bogdan.OnlineRetailShop.Dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bogdan.OnlineRetailShop.Model.Customer;
import com.bogdan.OnlineRetailShop.Model.Payment;

@Repository
@Transactional
public class PaymentDaoImpl implements PaymentDao {

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public void addPayment(Payment payment) {
		sessionFactory.getCurrentSession().save(payment);

	}

	@Override
	public List<Payment> getPaymentList() {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		@SuppressWarnings("unchecked")
		List<Payment> paymentList = session.createCriteria("from Payment").list();

		for (Payment payment : paymentList) {
			System.out.println(payment);
		}
		transaction.commit();
		return paymentList;
	}

	@Override
	public List<Payment> getPaymentListForCustomer(Customer customer) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		Query query = sessionFactory.getCurrentSession().createQuery("select * from Payment where customer= :param")
				.setParameter("param", customer);

		@SuppressWarnings("unchecked")
		List<Payment> paymentList = query.list();

		for (Payment payment : paymentList) {
			System.out.println(payment);
		}

		transaction.commit();
		return paymentList;
	}

}
