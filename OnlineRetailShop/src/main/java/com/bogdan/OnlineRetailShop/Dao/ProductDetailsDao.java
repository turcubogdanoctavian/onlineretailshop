package com.bogdan.OnlineRetailShop.Dao;

import com.bogdan.OnlineRetailShop.Model.Product;
import com.bogdan.OnlineRetailShop.Model.ProductDetails;

public interface ProductDetailsDao {
	public void addProductDetails(ProductDetails productDetails);

	public void updateProductDetails(ProductDetails productDetails);

	public void deleteProductDetails(ProductDetails productDetails);

	public ProductDetails getProductDetailsForProduct(Product product);
}
