package com.bogdan.OnlineRetailShop.Dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bogdan.OnlineRetailShop.Model.Employee;

@Repository
@Transactional
public class EmployeeDaoImpl implements EmployeeDao {

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public void addEmployee(Employee employee) {
		sessionFactory.getCurrentSession().save(employee);

	}

	@Override
	public void updateEmployee(Employee employee) {
		sessionFactory.getCurrentSession().update(employee);

	}

	@Override
	public void deleteEmployee(Employee employee) {
		sessionFactory.getCurrentSession().delete(employee);

	}

	@Override
	public List<Employee> getEmployeeList() {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		@SuppressWarnings("unchecked")
		List<Employee> employeeList = session.createCriteria("from Employee").list();

		for (Employee employee : employeeList) {
			System.out.println(employee);
		}

		transaction.commit();
		return employeeList;
	}

}
