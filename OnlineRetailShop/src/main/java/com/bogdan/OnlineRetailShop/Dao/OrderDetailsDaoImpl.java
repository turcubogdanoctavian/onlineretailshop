package com.bogdan.OnlineRetailShop.Dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bogdan.OnlineRetailShop.Model.Order;
import com.bogdan.OnlineRetailShop.Model.OrderDetails;

@Repository
@Transactional
public class OrderDetailsDaoImpl implements OrderDetailsDao {

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public void addOrderDetails(OrderDetails orderDetails) {
		sessionFactory.getCurrentSession().save(orderDetails);

	}

	@Override
	public void updateOrderDetails(OrderDetails orderDetails) {
		sessionFactory.getCurrentSession().update(orderDetails);

	}

	@Override
	public void deleteOrderDetails(OrderDetails orderDetails) {
		sessionFactory.getCurrentSession().delete(orderDetails);

	}

	@Override
	public List<OrderDetails> getOrderDetailsListForOrder(Order order) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		Query query = sessionFactory.getCurrentSession().createQuery("from OrderDetails where order= :param")
				.setParameter("param", order);

		@SuppressWarnings("unchecked")
		List<OrderDetails> orderDetailsList = query.list();

		for (OrderDetails orderDetails : orderDetailsList) {
			System.out.println(orderDetails);
		}

		transaction.commit();
		return orderDetailsList;
	}

}
