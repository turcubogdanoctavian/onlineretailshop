package com.bogdan.OnlineRetailShop.Dao;

import java.util.List;

import com.bogdan.OnlineRetailShop.Model.Customer;
import com.bogdan.OnlineRetailShop.Model.Order;

public interface OrderDao {
	public void addOrder(Order order);

	public void updateOrder(Order order);

	public void deleteOrder(Order order);

	public List<Order> getOrderList();

	public List<Order> getOrderListForCustomer(Customer customer);
}
