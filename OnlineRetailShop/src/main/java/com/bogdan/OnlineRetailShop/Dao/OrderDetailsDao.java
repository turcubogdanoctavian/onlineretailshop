package com.bogdan.OnlineRetailShop.Dao;

import java.util.List;

import com.bogdan.OnlineRetailShop.Model.Order;
import com.bogdan.OnlineRetailShop.Model.OrderDetails;

public interface OrderDetailsDao {
	public void addOrderDetails(OrderDetails orderDetails);

	public void updateOrderDetails(OrderDetails orderDetails);

	public void deleteOrderDetails(OrderDetails orderDetails);

	public List<OrderDetails> getOrderDetailsListForOrder(Order order);
}
