package com.bogdan.OnlineRetailShop.Model;

public class Employee {
	private int idEmployee;
	private String employeeName;
	private String employeeSurname;
	private String employeeEmail;
	private String employeeUsername;
	private String employeePassword;

	public Employee() {

	}

	public Employee(int idEmployee, String employeeName, String employeeSurname, String employeeEmail,
			String employeeUsername, String employeePassword) {
		super();
		this.idEmployee = idEmployee;
		this.employeeName = employeeName;
		this.employeeSurname = employeeSurname;
		this.employeeEmail = employeeEmail;
		this.employeeUsername = employeeUsername;
		this.employeePassword = employeePassword;
	}

	public int getIdEmployee() {
		return idEmployee;
	}

	public void setIdEmployee(int idEmployee) {
		this.idEmployee = idEmployee;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getEmployeeSurname() {
		return employeeSurname;
	}

	public void setEmployeeSurname(String employeeSurname) {
		this.employeeSurname = employeeSurname;
	}

	public String getEmployeeEmail() {
		return employeeEmail;
	}

	public void setEmployeeEmail(String employeeEmail) {
		this.employeeEmail = employeeEmail;
	}

	public String getEmployeeUsername() {
		return employeeUsername;
	}

	public void setEmployeeUsername(String employeeUsername) {
		this.employeeUsername = employeeUsername;
	}

	public String getEmployeePassword() {
		return employeePassword;
	}

	public void setEmployeePassword(String employeePassword) {
		this.employeePassword = employeePassword;
	}

	@Override
	public String toString() {
		return "Employee [idEmployee=" + idEmployee + ", employeeName=" + employeeName + ", employeeSurname="
				+ employeeSurname + ", employeeEmail=" + employeeEmail + ", employeeUsername=" + employeeUsername
				+ ", employeePassword=" + employeePassword + "]";
	}

}
