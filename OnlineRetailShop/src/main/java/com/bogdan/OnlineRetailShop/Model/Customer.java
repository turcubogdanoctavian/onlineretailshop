package com.bogdan.OnlineRetailShop.Model;

public class Customer {
	private int idCustomer;
	private String customerUsername;
	private String customerPassword;
	private String customerName;
	private String customerSurname;
	private int customerPhone;
	private String customerCounty;
	private String customerCity;
	private String customerAddress;
	private String customerEmail;

	public Customer() {

	}

	public Customer(int idCustomer, String customerUsername, String customerPassword, String customerName,
			String customerSurname, int customerPhone, String customerCounty, String customerCity,
			String customerAddress, String customerEmail) {
		super();
		this.idCustomer = idCustomer;
		this.customerUsername = customerUsername;
		this.customerPassword = customerPassword;
		this.customerName = customerName;
		this.customerSurname = customerSurname;
		this.customerPhone = customerPhone;
		this.customerCounty = customerCounty;
		this.customerCity = customerCity;
		this.customerAddress = customerAddress;
		this.customerEmail = customerEmail;
	}

	public int getIdCustomer() {
		return idCustomer;
	}

	public void setIdCustomer(int idCustomer) {
		this.idCustomer = idCustomer;
	}

	public String getCustomerUsername() {
		return customerUsername;
	}

	public void setCustomerUsername(String customerUsername) {
		this.customerUsername = customerUsername;
	}

	public String getCustomerPassword() {
		return customerPassword;
	}

	public void setCustomerPassword(String customerPassword) {
		this.customerPassword = customerPassword;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerSurname() {
		return customerSurname;
	}

	public void setCustomerSurname(String customerSurname) {
		this.customerSurname = customerSurname;
	}

	public int getCustomerPhone() {
		return customerPhone;
	}

	public void setCustomerPhone(int customerPhone) {
		this.customerPhone = customerPhone;
	}

	public String getCustomerCounty() {
		return customerCounty;
	}

	public void setCustomerCounty(String customerCounty) {
		this.customerCounty = customerCounty;
	}

	public String getCustomerCity() {
		return customerCity;
	}

	public void setCustomerCity(String customerCity) {
		this.customerCity = customerCity;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	@Override
	public String toString() {
		return "Customer [idCustomer=" + idCustomer + ", customerUsername=" + customerUsername + ", customerPassword="
				+ customerPassword + ", customerName=" + customerName + ", customerSurname=" + customerSurname
				+ ", customerPhone=" + customerPhone + ", customerCounty=" + customerCounty + ", customerCity="
				+ customerCity + ", customerAddress=" + customerAddress + ", customerEmail=" + customerEmail + "]";
	}

}
