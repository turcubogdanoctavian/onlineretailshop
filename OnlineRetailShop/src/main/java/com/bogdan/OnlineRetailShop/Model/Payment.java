package com.bogdan.OnlineRetailShop.Model;

import java.sql.Date;

public class Payment {
	private int idPayment;
	private Customer customer;
	private Date paymentDate;
	private double amount;

	public Payment() {

	}

	public Payment(int idPayment, Customer customer, Date paymentDate, double amount) {
		super();
		this.idPayment = idPayment;
		this.customer = customer;
		this.paymentDate = paymentDate;
		this.amount = amount;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Date getPayment() {
		return paymentDate;
	}

	public void setPayment(Date payment) {
		this.paymentDate = payment;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public int getIdPayment() {
		return idPayment;
	}

	public void setIdPayment(int idPayment) {
		this.idPayment = idPayment;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	@Override
	public String toString() {
		return "Payment [idPayment=" + idPayment + ", customer=" + customer + ", paymentDate=" + paymentDate
				+ ", amount=" + amount + "]";
	}

}
