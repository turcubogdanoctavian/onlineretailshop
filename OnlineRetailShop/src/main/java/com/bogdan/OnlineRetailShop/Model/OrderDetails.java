package com.bogdan.OnlineRetailShop.Model;

public class OrderDetails {
	private int idOrderDetails;
	private Order order;
	private Product product;
	private int qtyS;
	private int qtyM;
	private int qtyL;
	private int qtyXL;
	private double price;

	public OrderDetails() {

	}

	public OrderDetails(int idOrderDetails, Order order, Product product, int qtyS, int qtyM, int qtyL, int qtyXL,
			double price) {
		super();
		this.idOrderDetails = idOrderDetails;
		this.order = order;
		this.product = product;
		this.qtyS = qtyS;
		this.qtyM = qtyM;
		this.qtyL = qtyL;
		this.qtyXL = qtyXL;
		this.price = price;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getQtyS() {
		return qtyS;
	}

	public void setQtyS(int qtyS) {
		this.qtyS = qtyS;
	}

	public int getQtyM() {
		return qtyM;
	}

	public void setQtyM(int qtyM) {
		this.qtyM = qtyM;
	}

	public int getQtyL() {
		return qtyL;
	}

	public void setQtyL(int qtyL) {
		this.qtyL = qtyL;
	}

	public int getQtyXL() {
		return qtyXL;
	}

	public void setQtyXL(int qtyXL) {
		this.qtyXL = qtyXL;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getIdOrderDetails() {
		return idOrderDetails;
	}

	public void setIdOrderDetails(int idOrderDetails) {
		this.idOrderDetails = idOrderDetails;
	}

	@Override
	public String toString() {
		return "OrderDetails [idOrderDetails=" + idOrderDetails + ", order=" + order + ", product=" + product
				+ ", qtyS=" + qtyS + ", qtyM=" + qtyM + ", qtyL=" + qtyL + ", qtyXL=" + qtyXL + ", price=" + price
				+ "]";
	}

}
