package com.bogdan.OnlineRetailShop.Model;

public class Product {
	private int idProduct;
	private String productName;
	private ProductDetails productDetail;
	private double price;
	private int qtyS;
	private int qtyM;
	private int qtyL;
	private int qtyXL;
	private String gender;
	private Category category;

	public Product() {

	}

	public Product(int idProduct, String productName, ProductDetails productDetail, double price, int qtyS, int qtyM,
			int qtyL, int qtyXL, String gender, Category category) {
		super();
		this.idProduct = idProduct;
		this.productName = productName;
		this.productDetail = productDetail;
		this.price = price;
		this.qtyS = qtyS;
		this.qtyM = qtyM;
		this.qtyL = qtyL;
		this.qtyXL = qtyXL;
		this.gender = gender;
		this.category = category;
	}

	public int getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(int idProduct) {
		this.idProduct = idProduct;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public ProductDetails getProductDetails() {
		return productDetail;
	}

	public void setProductDetails(ProductDetails productDetail) {
		this.productDetail = productDetail;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getQtyS() {
		return qtyS;
	}

	public void setQtyS(int qtyS) {
		this.qtyS = qtyS;
	}

	public int getQtyM() {
		return qtyM;
	}

	public void setQtyM(int qtyM) {
		this.qtyM = qtyM;
	}

	public int getQtyL() {
		return qtyL;
	}

	public void setQtyL(int qtyL) {
		this.qtyL = qtyL;
	}

	public int getQtyXL() {
		return qtyXL;
	}

	public void setQtyXL(int qtyXL) {
		this.qtyXL = qtyXL;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return "Product [idProduct=" + idProduct + ", productName=" + productName + ", productDetail=" + productDetail
				+ ", price=" + price + ", qtyS=" + qtyS + ", qtyM=" + qtyM + ", qtyL=" + qtyL + ", qtyXL=" + qtyXL
				+ ", productGender=" + gender + ", category=" + category + "]";
	}

}
