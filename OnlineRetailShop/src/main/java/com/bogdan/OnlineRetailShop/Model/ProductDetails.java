package com.bogdan.OnlineRetailShop.Model;

public class ProductDetails {
	private int idProductDetails;
	private String productImage;
	private String productDescription;

	public ProductDetails() {

	}

	public int getIdProductDetails() {
		return idProductDetails;
	}

	public void setIdProductDetails(int idProductDetails) {
		this.idProductDetails = idProductDetails;
	}

	public ProductDetails(int idProductDetails, String productImage, String productDescription) {
		super();
		this.idProductDetails = idProductDetails;
		this.productImage = productImage;
		this.productDescription = productDescription;
	}

	public String getProductImage() {
		return productImage;
	}

	public void setProductImage(String productImage) {
		this.productImage = productImage;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	@Override
	public String toString() {
		return "ProductDetails [idProductDetails=" + idProductDetails + ", productImage=" + productImage
				+ ", productDescription=" + productDescription + "]";
	}

}
