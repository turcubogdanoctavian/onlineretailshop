package com.bogdan.OnlineRetailShop.Service;

import java.sql.Date;
import java.util.List;

import com.bogdan.OnlineRetailShop.Model.Customer;
import com.bogdan.OnlineRetailShop.Model.Order;

public interface OrderService {

	public void addOrder(Order order);

	public void updateOrder(int idOrder, Date orderDate, Date requiredDate, Date shippedDate, String status,
			Customer customer);

	public void deleteOrder(Order order);

	public List<Order> getOrderList();

	public List<Order> getOrderListForCustomer(Customer customer);

	public Order getOrderById(int id);

}
