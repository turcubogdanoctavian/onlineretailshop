package com.bogdan.OnlineRetailShop.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bogdan.OnlineRetailShop.Dao.EmployeeDao;
import com.bogdan.OnlineRetailShop.Model.Employee;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeDao employeeDao;

	@Override
	public void addEmployee(Employee employee) {
		employeeDao.addEmployee(employee);

	}

	@Override
	public void updateEmployee(int idEmployee, String employeeName, String employeeSurname, String employeeEmail,
			String employeeUsername, String employeePassword) {

		Employee employee = getEmployeeById(idEmployee);

		employee.setEmployeeName(employeeName);
		employee.setEmployeeSurname(employeeSurname);
		employee.setEmployeeEmail(employeeEmail);
		employee.setEmployeePassword(employeePassword);

	}

	@Override
	public void deleteEmployee(Employee employee) {
		employeeDao.deleteEmployee(employee);

	}

	@Override
	public List<Employee> getEmployeeList() {

		return employeeDao.getEmployeeList();
	}

	@Override
	public Employee getEmployeeById(int id) {
		for (Employee employee : employeeDao.getEmployeeList()) {
			if (employee.getIdEmployee() == id) {
				return employee;
			}
		}
		return null;
	}

}
