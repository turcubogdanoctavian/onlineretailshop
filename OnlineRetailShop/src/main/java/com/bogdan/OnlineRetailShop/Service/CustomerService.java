package com.bogdan.OnlineRetailShop.Service;

import java.util.List;

import com.bogdan.OnlineRetailShop.Model.Customer;

public interface CustomerService {

	public void addCustomer(Customer customer);

	public void updateCustomer(int idCustomer, String customerUsername, String customerPassword, String customerName,
			String customerSurname, int customerPhone, String customerCounty, String customerCity,
			String customerAddress, String customerEmail);

	public List<Customer> getCustomerList();

	public Customer getCustomerById(int id);

}
