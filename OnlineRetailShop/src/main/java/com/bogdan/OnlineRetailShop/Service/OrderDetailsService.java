package com.bogdan.OnlineRetailShop.Service;

import java.util.List;

import com.bogdan.OnlineRetailShop.Model.Order;
import com.bogdan.OnlineRetailShop.Model.OrderDetails;

public interface OrderDetailsService {

	public void addOrderDetails(OrderDetails orderDetails);

	public void updateOrderDetails(OrderDetails orderDetails);

	public void deleteOrderDetails(OrderDetails orderDetails);

	public List<OrderDetails> getOrderDetailsListForOrder(Order order);
}
