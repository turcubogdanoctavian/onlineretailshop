package com.bogdan.OnlineRetailShop.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bogdan.OnlineRetailShop.Dao.ProductDetailsDao;
import com.bogdan.OnlineRetailShop.Model.Product;
import com.bogdan.OnlineRetailShop.Model.ProductDetails;

@Service
@Transactional
public class ProductDetailsServiceImpl implements ProductDetailsService {

	@Autowired
	ProductDetailsDao productDetailsDao;

	@Override
	public void addProductDetails(ProductDetails productDetails) {
		productDetailsDao.addProductDetails(productDetails);

	}

	@Override
	public void updateProductDetails(Product product, String productImage, String productDescription) {
		ProductDetails productDetails = getProductDetailsForProduct(product);

		productDetails.setProductImage(productImage);
		productDetails.setProductDescription(productDescription);
	}

	@Override
	public void deleteProductDetails(ProductDetails productDetails) {
		productDetailsDao.deleteProductDetails(productDetails);

	}

	@Override
	public ProductDetails getProductDetailsForProduct(Product product) {
		return productDetailsDao.getProductDetailsForProduct(product);
	}

}
