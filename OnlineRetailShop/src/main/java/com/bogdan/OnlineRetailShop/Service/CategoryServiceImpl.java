package com.bogdan.OnlineRetailShop.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bogdan.OnlineRetailShop.Dao.CategoryDao;
import com.bogdan.OnlineRetailShop.Model.Category;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	CategoryDao categoryDao;

	@Override
	public void addCategory(Category category) {
		categoryDao.addCategory(category);

	}

	@Override
	public void updateCategory(int idCategory, String categoryName) {
		Category category = getCategoryById(idCategory);

		category.setCategoryName(categoryName);

	}

	@Override
	public void deteleCategory(Category category) {
		categoryDao.deteleCategory(category);

	}

	@Override
	public List<Category> getCategoryList() {

		return categoryDao.getCategoryList();
	}

	@Override
	public Category getCategoryById(int id) {
		for (Category category : categoryDao.getCategoryList()) {
			if (category.getIdCategory() == id) {
				return category;
			}
		}
		return null;
	}

}
