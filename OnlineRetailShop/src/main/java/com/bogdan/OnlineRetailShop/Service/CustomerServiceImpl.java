package com.bogdan.OnlineRetailShop.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bogdan.OnlineRetailShop.Dao.CustomerDao;
import com.bogdan.OnlineRetailShop.Model.Customer;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	CustomerDao customerDao;

	@Override
	public void addCustomer(Customer customer) {
		customerDao.addCustomer(customer);

	}

	@Override
	public void updateCustomer(int idCustomer, String customerUsername, String customerPassword, String customerName,
			String customerSurname, int customerPhone, String customerCounty, String customerCity,
			String customerAddress, String customerEmail) {

		Customer customer = getCustomerById(idCustomer);

		customer.setCustomerPassword(customerPassword);
		customer.setCustomerName(customerName);
		customer.setCustomerSurname(customerSurname);
		customer.setCustomerPhone(customerPhone);
		customer.setCustomerCounty(customerCounty);
		customer.setCustomerCity(customerCity);
		customer.setCustomerAddress(customerAddress);
		customer.setCustomerEmail(customerEmail);

	}

	@Override
	public List<Customer> getCustomerList() {

		return customerDao.getCustomerList();
	}

	@Override
	public Customer getCustomerById(int id) {
		for (Customer customer : customerDao.getCustomerList()) {
			if (customer.getIdCustomer() == id) {
				return customer;
			}
		}
		return null;
	}

}
