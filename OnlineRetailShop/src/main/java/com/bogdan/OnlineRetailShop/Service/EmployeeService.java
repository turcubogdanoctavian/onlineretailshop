package com.bogdan.OnlineRetailShop.Service;

import java.util.List;

import com.bogdan.OnlineRetailShop.Model.Employee;

public interface EmployeeService {

	public void addEmployee(Employee employee);

	public void updateEmployee(int idEmployee, String employeeName, String employeeSurname, String employeeEmail,
			String employeeUsername, String employeePassword);

	public void deleteEmployee(Employee employee);

	public List<Employee> getEmployeeList();

	public Employee getEmployeeById(int id);
}
