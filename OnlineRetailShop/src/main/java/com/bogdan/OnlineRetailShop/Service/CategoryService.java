package com.bogdan.OnlineRetailShop.Service;

import java.util.List;

import com.bogdan.OnlineRetailShop.Model.Category;

public interface CategoryService {

	public void addCategory(Category category);

	public void updateCategory(int idCategory, String categoryName);

	public void deteleCategory(Category category);

	public List<Category> getCategoryList();

	public Category getCategoryById(int id);
}
