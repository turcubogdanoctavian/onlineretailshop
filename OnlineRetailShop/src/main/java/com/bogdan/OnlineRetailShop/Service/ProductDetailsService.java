package com.bogdan.OnlineRetailShop.Service;

import com.bogdan.OnlineRetailShop.Model.Product;
import com.bogdan.OnlineRetailShop.Model.ProductDetails;

public interface ProductDetailsService {

	public void addProductDetails(ProductDetails productDetails);

	public void updateProductDetails(Product product, String productImage, String productDescription);

	public void deleteProductDetails(ProductDetails productDetails);

	public ProductDetails getProductDetailsForProduct(Product product);

}
