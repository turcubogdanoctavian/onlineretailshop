package com.bogdan.OnlineRetailShop.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bogdan.OnlineRetailShop.Dao.OrderDetailsDao;
import com.bogdan.OnlineRetailShop.Model.Order;
import com.bogdan.OnlineRetailShop.Model.OrderDetails;

@Service
@Transactional
public class OrderDetailsServiceImpl implements OrderDetailsService {

	@Autowired
	OrderDetailsDao orderDetailsDao;

	@Override
	public void addOrderDetails(OrderDetails orderDetails) {
		orderDetailsDao.addOrderDetails(orderDetails);

	}

	@Override
	public void updateOrderDetails(OrderDetails orderDetails) {
		orderDetailsDao.updateOrderDetails(orderDetails);

	}

	@Override
	public void deleteOrderDetails(OrderDetails orderDetails) {
		orderDetailsDao.deleteOrderDetails(orderDetails);

	}

	@Override
	public List<OrderDetails> getOrderDetailsListForOrder(Order order) {
		return orderDetailsDao.getOrderDetailsListForOrder(order);
	}

}
