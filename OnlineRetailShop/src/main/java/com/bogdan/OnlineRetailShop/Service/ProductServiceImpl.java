package com.bogdan.OnlineRetailShop.Service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bogdan.OnlineRetailShop.Dao.ProductDao;
import com.bogdan.OnlineRetailShop.Model.Category;
import com.bogdan.OnlineRetailShop.Model.Product;
import com.bogdan.OnlineRetailShop.Model.ProductDetails;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductDao productDao;

	@Override
	public void addProduct(Product product) {
		productDao.addProduct(product);

	}

	@Override
	public void updateProduct(int idProduct, String productName, ProductDetails productDetail, double price, int qtyS,
			int qtyM, int qtyL, int qtyXL, String productGender, Category category) {

		Product product = getProductbyId(idProduct);

		product.setProductName(productName);
		product.setProductDetails(productDetail);
		product.setPrice(price);
		product.setQtyS(qtyS);
		product.setQtyM(qtyM);
		product.setQtyL(qtyL);
		product.setQtyXL(qtyXL);
		product.setGender(productGender);
		product.setCategory(category);

	}

	@Override
	public void deleteProduct(Product product) {
		productDao.deleteProduct(product);

	}

	@Override
	public List<Product> getProductList() {

		return productDao.getProductList();
	}

	@Override
	public List<Product> getProductListForCategory(Category category) {
		return productDao.getProductListForCategory(category);
	}

	@Override
	public Product getProductbyId(int id) {
		for (Product product : productDao.getProductList()) {
			if (product.getIdProduct() == id) {
				return product;
			}
		}
		return null;
	}

	@Override
	public List<Product> searchForProduct(String productName) {
		List<Product> resultList = new ArrayList<>();

		for (Product product : productDao.getProductList()) {
			if (product.getProductName().contains(productName)) {
				resultList.add(product);
			}
		}
		return null;
	}

}
