package com.bogdan.OnlineRetailShop.Service;

import java.util.List;

import com.bogdan.OnlineRetailShop.Model.Category;
import com.bogdan.OnlineRetailShop.Model.Product;
import com.bogdan.OnlineRetailShop.Model.ProductDetails;

public interface ProductService {

	public void addProduct(Product product);

	public void updateProduct(int idProduct, String productName, ProductDetails productDetail, double price, int qtyS,
			int qtyM, int qtyL, int qtyXL, String productGender, Category category);

	public void deleteProduct(Product product);

	public List<Product> getProductList();

	public List<Product> getProductListForCategory(Category category);

	public Product getProductbyId(int id);
	
	public List <Product> searchForProduct(String productName);
}
