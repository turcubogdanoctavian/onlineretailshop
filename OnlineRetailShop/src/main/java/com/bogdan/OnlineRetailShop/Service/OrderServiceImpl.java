package com.bogdan.OnlineRetailShop.Service;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bogdan.OnlineRetailShop.Dao.OrderDao;
import com.bogdan.OnlineRetailShop.Model.Customer;
import com.bogdan.OnlineRetailShop.Model.Order;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

	@Autowired
	OrderDao orderDao;

	@Override
	public void addOrder(Order order) {
		orderDao.addOrder(order);

	}

	@Override
	public void updateOrder(int idOrder, Date orderDate, Date requiredDate, Date shippedDate, String status,
			Customer customer) {

		Order order = getOrderById(idOrder);

		order.setOrderDate(orderDate);
		order.setRequiredDate(requiredDate);
		order.setShippedDate(shippedDate);
		order.setStatus(status);
		order.setCustomer(customer);

	}

	@Override
	public void deleteOrder(Order order) {
		orderDao.deleteOrder(order);

	}

	@Override
	public List<Order> getOrderList() {
		return orderDao.getOrderList();
	}

	@Override
	public List<Order> getOrderListForCustomer(Customer customer) {
		return orderDao.getOrderListForCustomer(customer);
	}

	@Override
	public Order getOrderById(int id) {
		for (Order order : orderDao.getOrderList()) {
			if (order.getIdOrder() == id) {
				return order;
			}
		}
		return null;
	}

}
