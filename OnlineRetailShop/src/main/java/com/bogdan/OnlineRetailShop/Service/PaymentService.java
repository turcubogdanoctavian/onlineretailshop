package com.bogdan.OnlineRetailShop.Service;

import java.util.List;

import com.bogdan.OnlineRetailShop.Model.Customer;
import com.bogdan.OnlineRetailShop.Model.Payment;

public interface PaymentService {

	public void addPayment(Payment payment);

	public List<Payment> getPaymentList();

	public List<Payment> getPaymentListForCustomer(Customer customer);

}
