package com.bogdan.OnlineRetailShop.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bogdan.OnlineRetailShop.Dao.PaymentDao;
import com.bogdan.OnlineRetailShop.Model.Customer;
import com.bogdan.OnlineRetailShop.Model.Payment;

@Service
@Transactional
public class PaymentServiceImpl implements PaymentService {

	@Autowired
	PaymentDao paymentDao;

	@Override
	public void addPayment(Payment payment) {
		paymentDao.addPayment(payment);

	}

	@Override
	public List<Payment> getPaymentList() {

		return paymentDao.getPaymentList();
	}

	@Override
	public List<Payment> getPaymentListForCustomer(Customer customer) {

		return paymentDao.getPaymentListForCustomer(customer);
	}

}
